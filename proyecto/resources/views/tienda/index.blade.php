@extends('layouts.app')
@section('content')
<div class="container">
@if(Session::has('mensaje'))
{{Session::get('mensaje')}}
@endif

<ul class="nav nav-pills nav-fill">
    <li class="nav-item">
      <a class="nav-link" href="{{asset("usuario")}}">Usuarios</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{asset("producto")}}">Productos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link active" href="#!">Tiendas</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{asset("transporte")}}">Transporte</a>
    </li>
  </ul>
<a href="{{url('tienda/create')}}" class="btn btn-success" data-toggle="tooltip"
    data-placement="top" title="Nuevo producto">Registrar nueva tienda</a>
<br>
<br>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr >
            <th>ID</th>
            <th>Foto</th>
            <th>Nombre</th>
            <th>Dirección</th>
            <th>Horario</th>
            <th>Acciones</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($tiendas as $tienda)
            
        
        <tr >
            <td>{{$tienda->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$tienda->Foto}}"  width="150"alt=""class="Thumbnail image">
                
            </td>
            <td>{{$tienda->Nombre}}</td>
            <td>{{$tienda->Direccion}}</td>
            <td>{{$tienda->Horario}}</td>
            <td><a href="{{url('/tienda/'.$tienda->id.'/edit')}}" class="btn btn-warning" data-toggle="tooltip"
                data-placement="top" title="Editar">Editar</a> | 

                <form action="{{ url('/tienda/'.$tienda->id)}}" class="d-inline" method="POST">
                    @csrf
                    {{method_field('DELETE')}}
                <input type="submit" onclick="return confirm('Quieres eliminar?')" value="Borrar" class="btn btn-danger" data-toggle="tooltip"
                    data-placement="top" title="Borrar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection