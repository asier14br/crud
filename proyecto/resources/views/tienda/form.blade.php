<h1>{{$modo}} tienda</h1>

<div class="form-group">
<label for="Nombre">Nombre</label>
    <input  class="form-control" type="text" name="Nombre" value="{{isset ($tienda->Nombre)?$tienda->Nombre:''}}" id="Nombre">
   

    <label for="Direccion">Dirección</label>
    <input class="form-control"type="text" name="Direccion" value="{{isset ($tienda->Direccion)?$tienda->Direccion:''}}" id="Direccion">
    

    <label for="Horario">Horario</label>
    <input class="form-control"type="text" name="Horario" value="{{isset ($tienda->Horario)?$tienda->Horario:''}}"  id="Horario">
    

    
    @if (isset($tienda->Foto))
    <img src="{{asset('storage').'/'.$tienda->Foto}}" width="150" alt="" class="img-thumbnail img-fluid">    
    @endif
   
    <input type="file" name="Foto" value="" id="Foto" class="form-control">
    

    <input type="submit" value="{{$modo}} tienda" class="btn btn-success">
    <a href="{{url('tienda/')}}" class="btn btn-primary">Volver</a>
</div>