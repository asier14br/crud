<h1>{{$modo}} producto</h1>

<div class="form-group">
<label for="Nombre">Nombre</label>
    <input  class="form-control" type="text" name="Nombre" value="{{isset ($producto->Nombre)?$producto->Nombre:''}}" id="Nombre">
   

    <label for="Tipo">Tipo</label>
    <input class="form-control"type="text" name="Tipo" value="{{isset ($producto->Tipo)?$producto->Tipo:''}}" id="Tipo">
    

    <label for="Caducidad">Caducidad</label>
    <input class="form-control"type="text" name="Caducidad" value="{{isset ($producto->Caducidad)?$producto->Caducidad:''}}"  id="Caducidad">
    

    
    @if (isset($producto->Foto))
    <img src="{{asset('storage').'/'.$producto->Foto}}" width="150" alt="" class="img-thumbnail img-fluid">    
    @endif
   
    <input type="file" name="Foto" value="" id="Foto" class="form-control">
    

    <input type="submit" value="{{$modo}} producto" class="btn btn-success">
    <a href="{{url('producto/')}}" class="btn btn-primary">Volver</a>
</div>