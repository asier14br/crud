@extends('layouts.app')
@section('content')
<div class="container">
@if(Session::has('mensaje'))
{{Session::get('mensaje')}}
@endif

<ul class="nav nav-pills nav-fill">
    <li class="nav-item">
      <a class="nav-link active" href="#!">Usuarios</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{asset("producto")}}">Productos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{asset("tienda")}}">Tiendas</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{asset("transporte")}}">Transporte</a>
    </li>
  </ul>
<a href="{{url('usuario/create')}}" class="btn btn-success" data-toggle="tooltip"
    data-placement="top" title="Nuevo usuario">Registrar nuevo usuario</a>
<br>
<br>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr >
            <th>ID</th>
            <th>Foto</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Correo</th>
            <th>Acciones</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($usuarios as $usuario)
            
        
        <tr >
            <td>{{$usuario->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$usuario->Foto}}"  width="150"alt=""class="rounded-circle">
                
            </td>
            <td>{{$usuario->Nombre}}</td>
            <td>{{$usuario->Apellidos}}</td>
            <td>{{$usuario->Correo}}</td>
            <td><a href="{{url('/usuario/'.$usuario->id.'/edit')}}" class="btn btn-warning" data-toggle="tooltip"
                data-placement="top" title="Editar">Editar</a> | 

                <form action="{{ url('/usuario/'.$usuario->id)}}" class="d-inline" method="POST">
                    @csrf
                    {{method_field('DELETE')}}
                <input type="submit" onclick="return confirm('Quieres eliminar?')" value="Borrar" class="btn btn-danger" data-toggle="tooltip"
                    data-placement="top" title="Borrar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection