<h1>{{$modo}} usuario</h1>

<div class="form-group">
<label for="Nombre">Nombre</label>
    <input  class="form-control" type="text" name="Nombre" value="{{isset ($usuario->Nombre)?$usuario->Nombre:''}}" id="Nombre">
   

    <label for="Apellidos">Apellidos</label>
    <input class="form-control"type="text" name="Apellidos" value="{{isset ($usuario->Apellidos)?$usuario->Apellidos:''}}" id="Apellidos">
    

    <label for="Correo">Correo</label>
    <input class="form-control"type="text" name="Correo" value="{{isset ($usuario->Correo)?$usuario->Correo:''}}"  id="Correo">
    

    
    @if (isset($usuario->Foto))
    <img src="{{asset('storage').'/'.$usuario->Foto}}" width="150" alt="" class="img-thumbnail img-fluid">    
    @endif
   
    <input type="file" name="Foto" value="" id="Foto" class="form-control">
    

    <input type="submit" value="{{$modo}} usuario" class="btn btn-success">
    <a href="{{url('usuario/')}}" class="btn btn-primary">Volver</a>
</div>