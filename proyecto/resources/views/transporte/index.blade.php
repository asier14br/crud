@extends('layouts.app')
@section('content')
<link rel="stylesheet" type="text/css" href='{{asset("css/app.css")}}'>

<div class="container">
@if(Session::has('mensaje'))
{{Session::get('mensaje')}}
@endif

<ul class="nav nav-pills nav-fill">
    <li class="nav-item">
      <a class="nav-link" href="{{asset("usuario")}}">Usuarios</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{asset("producto")}}">Productos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{asset("tienda")}}">Tiendas</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="#">Transporte</a>
    </li>
  </ul>
<a href="{{url('transporte/create')}}" class="btn btn-success" data-toggle="tooltip"
    data-placement="top" title="Nuevo transporte">Registrar nuevo transporte</a>
<br>
<br>
<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr >
            <th>ID</th>
            <th>Foto</th>
            <th>Modelo</th>
            <th>Matricula</th>
            <th>Capacidad</th>
            <th>Acciones</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($transportes as $transporte)
            
        
        <tr >
            <td>{{$transporte->id}}</td>
            <td>
                <img src="{{asset('storage').'/'.$transporte->Foto}}"  width="150"alt=""class="Thumbnail image">
                
            </td>
            <td>{{$transporte->Modelo}}</td>
            <td>{{$transporte->Matricula}}</td>
            <td>{{$transporte->Capacidad}}</td>
            <td><a href="{{url('/transporte/'.$transporte->id.'/edit')}}" class="btn btn-warning" data-toggle="tooltip"
                data-placement="top" title="Editar">Editar</a> | 

                <form action="{{ url('/transporte/'.$transporte->id)}}" class="d-inline" method="POST">
                    @csrf
                    {{method_field('DELETE')}}
                <input type="submit" onclick="return confirm('Quieres eliminar?')" value="Borrar" class="btn btn-danger" data-toggle="tooltip"
                    data-placement="top" title="Borrar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection