<h1>{{$modo}} transporte</h1>

<div class="form-group">
<label for="Modelo">Modelo</label>
    <input  class="form-control" type="text" name="Modelo" value="{{isset ($transporte->Modelo)?$transporte->Modelo:''}}" id="Modelo">
   

    <label for="Matricula">Matricula</label>
    <input class="form-control"type="text" name="Matricula" value="{{isset ($transporte->Matricula)?$transporte->Matricula:''}}" id="Matricula">
    

    <label for="Capacidad">Capacidad</label>
    <input class="form-control"type="text" name="Capacidad" value="{{isset ($transporte->Capacidad)?$transporte->Capacidad:''}}"  id="Capacidad">
    

    
    @if (isset($transporte->Foto))
    <img src="{{asset('storage').'/'.$transporte->Foto}}" width="150" alt="" class="img-thumbnail img-fluid">    
    @endif
   
    <input type="file" name="Foto" value="" id="Foto" class="form-control">
    

    <input type="submit" value="{{$modo}} transporte" class="btn btn-success">
    <a href="{{url('transporte/')}}" class="btn btn-primary">Volver</a>
</div>