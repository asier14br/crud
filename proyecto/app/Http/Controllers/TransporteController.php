<?php

namespace App\Http\Controllers;

use App\Models\Transporte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TransporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['transportes']=Transporte::paginate(5);
        return view('transporte.index', $datos);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('transporte.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosTransporte=request()->except('_token');
        if($request->hasFile('Foto')){
            $datosTransporte['Foto']=$request->file('Foto')->store('uploads','public');
        }
        Transporte::insert($datosTransporte);
        return  redirect('transporte')->with('mensaje','Transporte registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Transporte $transporte)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $transporte=Transporte::findOrFail($id);

        return view('transporte.edit', compact('transporte'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datosTransporte=request()->except(['_token','_method']);
        if($request->hasFile('Foto')){
            $transporte=Transporte::findOrFail($id);

            Storage::delete('public/'.$transporte->Foto);
            $datosTransporte['Foto']=$request->file('Foto')->store('uploads','public');
        }
        Transporte::where('id','=',$id)->update($datosTransporte);
        $transporte=Transporte::findOrFail($id);
        return view('transporte.edit', compact('transporte'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $transporte=Transporte::findOrFail($id);
        if(Storage::delete('public/'.$transporte->Foto)){
            Transporte::destroy($id);

        }
        return  redirect('transporte')->with('mensaje','Transporte eliminado correctamente');

    }
}
