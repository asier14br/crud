<?php

namespace App\Http\Controllers;

use App\Models\Tienda;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['tiendas']=Tienda::paginate(5);
        return view('tienda.index', $datos);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('tienda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosTienda=request()->except('_token');
        if($request->hasFile('Foto')){
            $datosTienda['Foto']=$request->file('Foto')->store('uploads','public');
        }
        Tienda::insert($datosTienda);
        return  redirect('tienda')->with('mensaje','Tienda registrada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Tienda $tienda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tienda=Tienda::findOrFail($id);

        return view('tienda.edit', compact('tienda'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datosTienda=request()->except(['_token','_method']);
        if($request->hasFile('Foto')){
            $tienda=Tienda::findOrFail($id);

            Storage::delete('public/'.$tienda->Foto);
            $datosTienda['Foto']=$request->file('Foto')->store('uploads','public');
        }
        Tienda::where('id','=',$id)->update($datosTienda);
        $tienda=Tienda::findOrFail($id);
        return view('tienda.edit', compact('producto'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $tienda=Tienda::findOrFail($id);
        if(Storage::delete('public/'.$tienda->Foto)){
            Tienda::destroy($id);

        }
        return  redirect('tienda')->with('mensaje','Tienda eliminada correctamente');

    }
}
