<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\TiendaController;
use App\Http\Controllers\TransporteController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
Route::get('/', function () {
    return view('auth.login');
});
/*
Route::get('/', function () {
    return view('welcome');
});
Route::get('/usuario', function () {
    return view('usuario.index');
});*/

Route::resource('usuario',UsuarioController::class )->middleware('auth');
Auth::routes();

Route::get('/home', [UsuarioController::class, 'index'])->name('home');
Route::prefix(['middleware'=>'auth'],function() {
    Route::get('/', [UsuarioController::class, 'index'])->name('home');

});
  




Route::resource('producto',ProductoController::class )->middleware('auth');
Auth::routes();

Route::get('/producto', [ProductoController::class, 'index'])->name('producto');
Route::prefix(['middleware'=>'auth'],function() {
    Route::get('/', [ProductoController::class, 'index'])->name('producto');

});




Route::resource('tienda',TiendaController::class )->middleware('auth');
Auth::routes();

Route::get('/tienda', [TiendaController::class, 'index'])->name('tienda');
Route::prefix(['middleware'=>'auth'],function() {
    Route::get('/', [TiendaController::class, 'index'])->name('tienda');

});





Route::resource('transporte',TransporteController::class )->middleware('auth');
Auth::routes();

Route::get('/transporte', [TransporteController::class, 'index'])->name('transporte');
Route::prefix(['middleware'=>'auth'],function() {
    Route::get('/', [TransporteController::class, 'index'])->name('transporte');

});